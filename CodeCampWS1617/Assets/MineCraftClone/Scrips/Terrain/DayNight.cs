﻿using UnityEngine;
using System.Collections;

public class DayNight : MonoBehaviour {

    float time = 2f;
    float currentTime = 0f;
    Quaternion rotation;
    public GameObject stars;
	
	void Update () {
        currentTime = time * Time.deltaTime;
        float angle = time * Time.deltaTime;
        transform.RotateAround(Vector3.zero, Vector3.right, angle );

        /*rotation = GetComponent<Transform>().rotation;
        Debug.Log("Roation: " + rotation.x);
        if (rotation.x > 0 && rotation.x <= 90)
        {
            stars.SetActive(false);
        }
        else
        {
            stars.SetActive(true);
        }
        */
        transform.LookAt(Vector3.zero);
	}
}
