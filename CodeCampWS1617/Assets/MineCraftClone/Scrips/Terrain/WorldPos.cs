﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public struct WorldPos
{
    #region Fields

    //Position of a world (x, y, z)
    public int x, y, z;

    #endregion Fields

    #region Constructor

    //Constructor for new new instance of WorldPos
    public WorldPos(int x, int y, int z)
    {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    #endregion Constructor

    /*
     * Generates a unique integer for any WorldPos, 
     * it's not actually completely unique though, 
     * it would be possible for two positions to get the same hash but very unlikely
     * 
     * To make the hash as unique as possible 
     * we start it as a prime number and multiply it by another prime number
     */
    public override int GetHashCode()
    {
        unchecked
        {
            int hash = 47;

            hash = hash * 227 + x.GetHashCode();
            hash = hash * 227 + y.GetHashCode();
            hash = hash * 227 + z.GetHashCode();

            return hash;
        }
    }

    //Compare the hashCode
    public override bool Equals(object obj)
    {
        if (GetHashCode() == obj.GetHashCode())
            return true;
        return false;
    }
}