﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class World : MonoBehaviour
{

    #region Fields

    /* Dictionary with WorldPos and Chunk as the value
     * to store many chunks
     */
    public Dictionary<WorldPos, Chunk> chunks = new Dictionary<WorldPos, Chunk>();

    //Prefab of our chunk
    public GameObject chunkPrefab;

    //Name of world for saving
    public string worldName = "world";
    public long seed = 0;
    public TerrainGen.TYPES type = TerrainGen.TYPES.STANDARD;
    public TerrainGen.BIOMES biome = TerrainGen.BIOMES.STANDARD;
    TerrainGen terrainGen;

    public List<WorldPos> seas;

    #endregion Fields

    void Start()
    {
        terrainGen = new TerrainGen();
        terrainGen.SetDataFromSeed(this);
    }

    /// <summary>
    /// Create a new chunk with TerrainGen
    /// Add the chunk the given worldPos
    /// in the Dictionary 
    /// </summary>
    /// <param name="x">X-Coordinate</param>
    /// <param name="y">Y-Coordinate</param>
    /// <param name="z">Z-Coordinate</param>
    public void CreateChunk(int x, int y, int z)
    {
        WorldPos worldPos = new WorldPos(x, y, z);

        //Instantiate the chunk at the coordinates using the chunk prefab
        GameObject newChunkObject = Instantiate(
                        chunkPrefab, new Vector3(x, y, z),
                        Quaternion.Euler(Vector3.zero)
                    ) as GameObject;

        Chunk newChunk = newChunkObject.GetComponent<Chunk>();

        newChunk.pos = worldPos;
        newChunk.world = this;

        //Add it to the chunks dictionary with the position as the key
        chunks.Add(worldPos, newChunk);

        newChunk = terrainGen.ChunkGen(newChunk);

        foreach (WorldPos sea in newChunk.seas)
        {
            Vector3 source = new Vector3(sea.x, sea.y, sea.z);
            StartCoroutine(SimulateWater(sea, source, 100));
        }


        newChunk.SetBlocksUnmodified();

        Serialization.Load(newChunk);
    }

    /// <summary>
    /// Takes the coordinates of a chunk
    /// and removes it from the world
    /// and from our dictionary after saving
    /// </summary>
    /// <param name="x">X-Coordinate</param>
    /// <param name="y">Y-Coordinate</param>
    /// <param name="z">Z-Coordinate</param>
    public void DestroyChunk(int x, int y, int z)
    {
        Chunk chunk = null;
        if (chunks.TryGetValue(new WorldPos(x, y, z), out chunk))
        {
            Serialization.SaveChunk(chunk);
            Object.Destroy(chunk.gameObject);
            chunks.Remove(new WorldPos(x, y, z));
        }
    }

    #region Getter

    /// <summary>
    /// Returns the searched Chunk if found in Dictionary
    /// </summary>
    /// <param name="x">X-Coordinate</param>
    /// <param name="y">Y-Coordinate</param>
    /// <param name="z">Z-Coordinate</param>
    /// <returns>Returns the searched Chunk if found in Dictionary</returns>
    public Chunk GetChunk(int x, int y, int z)
    {
        WorldPos pos = new WorldPos();
        float multiple = Chunk.chunkSize;

        //FloorToInt if chunkSize (multiple) is a float
        pos.x = Mathf.FloorToInt(x / multiple) * Chunk.chunkSize;
        pos.y = Mathf.FloorToInt(y / multiple) * Chunk.chunkSize;
        pos.z = Mathf.FloorToInt(z / multiple) * Chunk.chunkSize;

        Chunk containerChunk = null;

        //TryGetValue to get the chunk with given position
        chunks.TryGetValue(pos, out containerChunk);

        return containerChunk;
    }

    /// <summary>
    /// Return the searched Block with given position
    /// </summary>
    /// <param name="x">X-Coordinate</param>
    /// <param name="y">Y-Coordinate</param>
    /// <param name="z">Z-Coordinate</param>
    /// <returns>Block at X, Y, Z Coordinate</returns>
    public Block GetBlock(int x, int y, int z)
    {
        Chunk containerChunk = GetChunk(x, y, z);

        if (containerChunk != null)
        {
            Block block = containerChunk.GetBlock(
                x - containerChunk.pos.x,
                y - containerChunk.pos.y,
                z - containerChunk.pos.z);

            return block;
        }
        else
        {
            return new BlockAir();
        }

    }

    #endregion Getter

    #region Setter

    public void SetBlock(int x, int y, int z, Block block)
    {
        Chunk chunk = GetChunk(x, y, z);

        if (chunk != null)
        {
            chunk.SetBlock(x - chunk.pos.x, y - chunk.pos.y, z - chunk.pos.z, block);
            chunk.update = true;

            //Subtract the block position from the value to get the block's local position
            UpdateIfEqual(x - chunk.pos.x, 0, new WorldPos(x - 1, y, z));
            UpdateIfEqual(x - chunk.pos.x, Chunk.chunkSize - 1, new WorldPos(x + 1, y, z));
            UpdateIfEqual(y - chunk.pos.y, 0, new WorldPos(x, y - 1, z));
            UpdateIfEqual(y - chunk.pos.y, Chunk.chunkSize - 1, new WorldPos(x, y + 1, z));
            UpdateIfEqual(z - chunk.pos.z, 0, new WorldPos(x, y, z - 1));
            UpdateIfEqual(z - chunk.pos.z, Chunk.chunkSize - 1, new WorldPos(x, y, z + 1));
        
        }
    }

    #endregion Setter

    /// <summary>
    /// Check if value1 equals value2 to update the chunk conatining the position
    /// </summary>
    /// <param name="value1">Value1</param>
    /// <param name="value2">Value2</param>
    /// <param name="pos">Position of the chunk</param>
    void UpdateIfEqual(int value1, int value2, WorldPos pos)
    {
        if (value1 == value2)
        {
            Chunk chunk = GetChunk(pos.x, pos.y, pos.z);
            if (chunk != null)
                chunk.update = true;
        }
    }

    /*
     * SimulateWater start a Coroutine for recursiv
     * simulation of water blocks
     */
    public IEnumerator SimulateWater(WorldPos pos, Vector3 source, int maxDistance)
    {
        yield return new WaitForSeconds(0.5f);

        Block bottomBlock = GetBlock(pos.x, pos.y - 1, pos.z);

        if (bottomBlock is BlockAir)
        {
            SetBlock(pos.x, pos.y - 1, pos.z, new BlockWater());
            WorldPos newPos = new WorldPos(pos.x, pos.y - 1, pos.z);

            Vector3 newSource = new Vector3(newPos.x, newPos.y, newPos.z);

            StartCoroutine(SimulateWater(newPos, newSource, maxDistance));
        }
        else if (!(bottomBlock is BlockWater))
        {
            Block forwardBlock = GetBlock(pos.x, pos.y, pos.z + 1);

            if (forwardBlock is BlockAir)
            {

                SetBlock(pos.x, pos.y, pos.z + 1, new BlockWater());
                WorldPos newPos = new WorldPos(pos.x, pos.y, pos.z + 1);

                Vector3 newVector = new Vector3(newPos.x, newPos.y, newPos.z);

                Vector3 distance = newVector - source;

                if (distance.magnitude < maxDistance || bottomBlock is BlockWater)
                {
                    StartCoroutine(SimulateWater(newPos, source, maxDistance));
                }
            }

            Block backwardBlock = GetBlock(pos.x, pos.y, pos.z - 1);

            if (!backwardBlock.waterResistant)
            {
                SetBlock(pos.x, pos.y, pos.z - 1, new BlockWater());
                WorldPos newPos = new WorldPos(pos.x, pos.y, pos.z - 1);

                Vector3 newVector = new Vector3(newPos.x, newPos.y, newPos.z);

                Vector3 distance = newVector - source;

                if (distance.magnitude < maxDistance || bottomBlock is BlockWater)
                {
                    StartCoroutine(SimulateWater(newPos, source, maxDistance));
                }
            }

            Block rightBlock = GetBlock(pos.x + 1, pos.y, pos.z);

            if (!rightBlock.waterResistant)
            {
                SetBlock(pos.x + 1, pos.y, pos.z, new BlockWater());
                WorldPos newPos = new WorldPos(pos.x + 1, pos.y, pos.z);

                Vector3 newVector = new Vector3(newPos.x, newPos.y, newPos.z);

                Vector3 distance = newVector - source;

                if (distance.magnitude < maxDistance || bottomBlock is BlockWater)
                {
                    StartCoroutine(SimulateWater(newPos, source, maxDistance));
                }
            }

            Block leftBlock = GetBlock(pos.x - 1, pos.y, pos.z);

            if (!leftBlock.waterResistant)
            {
                SetBlock(pos.x - 1, pos.y, pos.z, new BlockWater());
                WorldPos newPos = new WorldPos(pos.x - 1, pos.y, pos.z);

                Vector3 newVector = new Vector3(newPos.x, newPos.y, newPos.z);

                Vector3 distance = newVector - source;

                if (distance.magnitude < maxDistance || bottomBlock is BlockWater)
                {
                    StartCoroutine(SimulateWater(newPos, source, maxDistance));
                }
            }
        }
    }

    #region Menüeinbindung

#if UNITY_EDITOR
    [MenuItem("GameObject/World")]
    public static World CreateWorld()
    {
        GameObject g0 = new GameObject("World");
        World world = g0.AddComponent<World>();
        return world;
    }
#endif

    #endregion Menüeinbindung
}
