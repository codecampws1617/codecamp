﻿using UnityEngine;
using System.Collections;
using SimplexNoise;
using System;
using Random = UnityEngine.Random;

public class TerrainGen
{

    #region Fields
    public enum TYPES { STANDARD, FLATLAND, AMPLIFIED }
    public enum BIOMES { STANDARD, SNOW, DESERT, LSD}

    #region Base Layer Fields
    float baseHeight = -24f;
    float baseNoise = 0.05f;
    float baseNoiseHeight = 7f;
    #endregion Base Layer Fields

    #region Mountain Layer Fields
    float mountainHeight = 5f;
    float mountainFrequency = 0.008f;
    float minHeight = -12f;
    #endregion Mountain Layer Fields

    #region Top Layer Fields
    float topBaseHeight = 1f;
    float topNoise = 1f;
    float topNoiseHeight = 1f;
    #endregion Top Layer Fields

    #region Flora Fields
    float treeFrequency = 0.2f;
    int treeDensity = 1;

    float seaFrequency = 0.2f;
    int seaDensity = 10;
    #endregion Flora Fields

    DungeonGenerator dungeon = GameObject.FindObjectOfType<DungeonGenerator>();
    bool dungeonGenerated = false;

    #endregion Fields

    /*
     * Split the chunk column and call for every chunk ChunkColumnGen
     */
    public Chunk ChunkGen(Chunk chunk)
    {
        //cycle over every column of chunk
        for (int x = chunk.pos.x - 3; x < chunk.pos.x + Chunk.chunkSize + 3; x++) //Change this line
        {
            for (int z = chunk.pos.z - 3; z < chunk.pos.z + Chunk.chunkSize + 3; z++)//and this line
            {
                chunk = ChunkColumnGen(chunk, x, z);
            }
        }
        return chunk;
    }

    /*
     * Fill the columns of chunk
     */
    public Chunk ChunkColumnGen(Chunk chunk, int x, int z)
    {
        /*
         * Raise stone height variable set to the base height,
         * add the mountain noise then raise everything 
         * under the minimum to the minimum and apply the base noise
         */
        int stoneHeight = Mathf.FloorToInt(baseHeight);
        stoneHeight += GetNoise(x, 0, z, mountainFrequency, Mathf.FloorToInt(mountainHeight));
        if (stoneHeight < minHeight)
            stoneHeight = Mathf.FloorToInt(minHeight);
        stoneHeight += GetNoise(x, 0, z, baseNoise, Mathf.FloorToInt(baseNoiseHeight));

        /*
         * Set top height variable equal to the stone height 
         * plus the base top height and add the top noise on top
         */
        int topHeight = stoneHeight + Mathf.FloorToInt(topBaseHeight);
        topHeight += GetNoise(x, 100, z, topNoise, Mathf.FloorToInt(topNoiseHeight));

        /*
         * Cycle through every chunk in the column adding block that matching the conditions
         */ 
        for (int y = chunk.pos.y - 8; y < chunk.pos.y + Chunk.chunkSize; y++)
        {
            //Fill column with base block
            if (y <= stoneHeight)
            {
                switch (chunk.world.biome)
                {
                    case BIOMES.STANDARD:
                        SetBlock(x, y, z, new Block(), chunk);
                        break;
                    case BIOMES.SNOW:
                        SetBlock(x, y, z, new BlockIce(), chunk);
                        break;
                    case BIOMES.DESERT:
                        SetBlock(x, y, z, new BlockSandstone(), chunk);
                        break;
                    case BIOMES.LSD:
                        SetBlock(x, y, z, new BlockLSD(), chunk);
                        break;
                }
            }
            //Fill coulumn with top and create tree 
            else if (y <= topHeight)
            {
                if (chunk.world.type != TYPES.FLATLAND)
                {
                    if (y == minHeight + 1 && GetNoise(x, 0, z, seaFrequency, 100) < seaDensity)
                    {
                        Block blockRight = chunk.world.GetBlock(x + 1, y, z);
                        Block blockLeft = chunk.world.GetBlock(x - 1, y, z);
                        Block blockNorth = chunk.world.GetBlock(x, y, z + 1);
                        Block blockSouth = chunk.world.GetBlock(x, y, z - 1);

                        if (blockSouth != null && blockRight != null && blockNorth != null && blockLeft != null)
                        {
                            if (!(blockRight.waterResistant) && !(blockLeft.waterResistant) && !(blockNorth.waterResistant) && !(blockSouth.waterResistant))
                            {
                                chunk.seas.Add(new WorldPos(x, y + 1, z));
                            }
                        }
                    }
                }
                

                switch (chunk.world.biome)
                {
                    case BIOMES.STANDARD:
                        SetBlock(x, y, z, new BlockGrass(), chunk);
                        int random = Random.Range(0, 100);

                        if (random < 5)
                        {
                            SetBlock(x, y + 1, z, new BlockCulm(), chunk, true);
                        }
                        else if (random > 6 && random < 10)
                        {
                            if (random == 7)
                            {
                                SetBlock(x, y + 1, z, new BlockFlower2(), chunk, true);
                            }
                            else
                            {
                                SetBlock(x, y + 1, z, new BlockFlower(), chunk, true);
                            }  
                        }
                        break;
                    case BIOMES.SNOW:
                        SetBlock(x, y, z, new BlockFullSnow(), chunk);
                        SetBlock(x, y + 1, z, new BlockSnow(), chunk);
                        break;
                    case BIOMES.DESERT:
                        SetBlock(x, y, z, new BlockSand(), chunk);
                        break;
                    case BIOMES.LSD:
                        SetBlock(x, y, z, new BlockLSD(), chunk);
                        break;

                }                

                if (y == topHeight && GetNoise(x, 0, z, treeFrequency, 100) < treeDensity)
                {
                    switch (chunk.world.biome)
                    {
                        case BIOMES.STANDARD:
                            CreateTree(x, y + 1, z, chunk);
                            break;
                        case BIOMES.DESERT:
                            CreateCactus(x, y + 1, z, chunk);
                            break;
                        case BIOMES.SNOW:
                            CreateIceFlower(x, y + 1, z, chunk);
                            break;
                        case BIOMES.LSD:
                            SetBlock(x, y, z, new BlockLSD(), chunk);
                            break;
                    }
                }
            }
            //Fill the rest of chunk column with BlockAir
            else
            {
                SetBlock(x, y, z, new BlockAir(), chunk);
            }

            if (!dungeonGenerated)
            {
                if (dungeon.isOnMap(chunk.pos.x, chunk.pos.z))
                {
                    dungeon.DungeonChunk(chunk.pos.x, -50, chunk.pos.z);

                    dungeonGenerated = true;
                }
            }

        }

        return chunk;
    }

    #region CreateFlora
    /*
     * Method to place an IceFlower to position
     */
    void CreateIceFlower(int x, int y, int z, Chunk chunk)
    {
        
        for (int yt = 0; yt < 1; yt++)
        {
            SetBlock(x, y + yt, z, new BlockIceFlower(), chunk, true);
        }
    }

    /*
     * Method to place culm to position
     */
    void CreateCulm(int x, int y, int z, Chunk chunk)
    {

        for (int yt = 0; yt < 1; yt++)
        {
            SetBlock(x, y + yt, z, new BlockCulm(), chunk, true);
        }
    }

    /*
     * Method to place a tree to position
     */
    void CreateTree(int x, int y, int z, Chunk chunk)
    {
        //create leaves, pretty leaves

        int trunkHeight = Random.Range(4, 8);

        int radius = Random.Range(3, 4);

        Vector3 center = new Vector3(x, y + (trunkHeight - 1), z);
        for (int xi = -radius; xi <= radius; xi++)
        {
            for (int yi = (trunkHeight - radius); yi <= (trunkHeight + radius); yi++)
            {
                for (int zi = -radius; zi <= radius; zi++)
                {
                    Vector3 position = new Vector3(x + xi, y + yi, z + zi);
                    float distance = Vector3.Distance(position, center);
                    if (distance < radius)
                    {
                        SetBlock(x + xi, y + yi, z + zi, new BlockLeaves(), chunk, true);
                    }

                }
            }
        }


        //create trunk
        for (int yt = 0; yt < trunkHeight; yt++)
        {
            SetBlock(x, y + yt, z, new BlockWood(), chunk, true);
        }
    }

    /*
     * Method to place a cactus to position
     */
    void CreateCactus(int x, int y, int z, Chunk chunk)
        {
            int cactusHeight = UnityEngine.Random.Range(3, 7);
            int random = UnityEngine.Random.Range(0, 10);

            if (random < 5)
            {
                for (int yt = 0; yt < cactusHeight; yt++)
                {
                    SetBlock(x, y + yt, z, new BlockCactus(), chunk, true);
                }
            }
            else if(random > 5)
            {
                for (int yt = 0; yt < cactusHeight; yt++)
                {
                    SetBlock(x, y + yt, z, new BlockSugarCane(), chunk, true);
                }  
            
            }
    }

    #endregion CreateFlora

    /*
     * Method to place a block to position
     */
    public static void SetBlock(int x, int y, int z, Block block, Chunk chunk, bool replaceBlocks = false)
    {
        x -= chunk.pos.x;
        y -= chunk.pos.y;
        z -= chunk.pos.z;

        if (Chunk.InRange(x) && Chunk.InRange(y) && Chunk.InRange(z))
        {
            if (replaceBlocks || chunk.blocks[x, y, z] == null)
                chunk.SetBlock(x, y, z, block);
        }
    }    

    #region Helpermethods

    /*
     * Call the given Simplex Noise method to get some noise for the coordinates
     * Scale value multiply the coordinates by, 
     * a smaller value here samples a smaller area giving us smooth noise suitable for mountains with long distances 
     * between the peaks and valleys but a larger value will make more frequent bumps and dips
     * 
     * The max parameter is the max of the return value, we will always get an int returned between zero and max
     */
    public static int GetNoise(int x, int y, int z, float scale, int max)
    {
        return Mathf.FloorToInt((Noise.Generate(x * scale, y * scale, z * scale) + 1f) * (max / 2f));
    }

    /*
     * Multiply and set properties of Generation with given seed
     */
    public void SetDataFromSeed(World world)
    {
        String seed = world.seed.ToString();

        seed.Replace("-", "");

        if (seed.Length <= 1)
        {
            long randomSeed = (long)(Random.value * 10000);

            seed = randomSeed.ToString();
        }
        else if (seed.Length > 4)
        {
            seed = seed.Substring(0, 4);
        }
        else if (seed.Length < 4)
        {
            int concat = 4 - seed.Length;

            for (int i = 0; i <= concat; i++)
            {
                seed += "1";
            }

        }

        seed = seed.Substring(0, 4);

        seed.Replace("0", "1");

        switch (world.type)
        {
            case TYPES.STANDARD:
                mountainHeight *= int.Parse(seed.Substring(0, 1));
                baseNoiseHeight = int.Parse(seed.Substring(1, 1)) + 5;
                treeDensity = int.Parse(seed.Substring(2, 1)) + 5;
                break;

            case TYPES.AMPLIFIED:
                baseNoiseHeight = 64 - mountainHeight - int.Parse(seed.Substring(1, 1));

                Debug.Log(baseNoiseHeight);
                Debug.Log(mountainHeight);

                treeDensity = int.Parse(seed.Substring(2, 1)) + 5;

                seaDensity = 50;
                break;

            case TYPES.FLATLAND:
                baseNoiseHeight = 1f;
                mountainHeight = 1f;
                topBaseHeight = 1f;
                topNoise = 1f;
                topNoiseHeight = 1f;
                treeDensity = int.Parse(seed.Substring(2, 1)) + 5;
                break;
        }
    }

    #endregion Helpermethods
}