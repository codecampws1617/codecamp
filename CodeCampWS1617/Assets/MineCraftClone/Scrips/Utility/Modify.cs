﻿using UnityEngine;
using System.Collections;

public class Modify : MonoBehaviour
{
    void Update()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            RaycastHit hit;
            if (Physics.Raycast(transform.position, ray.direction, out hit, 100))
            {
                if (hit.distance < 5)
                {
                    EditTerrain.SetBlock(hit, new BlockAir());
                }
            }
        }

        if (Input.GetKeyDown(KeyCode.Mouse1))
        {
            RaycastHit hit;

            if (Physics.Raycast(transform.position, ray.direction, out hit, 100))
            {
                if (hit.distance < 5)
                {
                    WorldPos currentBlockPos = EditTerrain.GetBlockPos(hit);
                    WorldPos source = new WorldPos(currentBlockPos.x, currentBlockPos.y + 1, currentBlockPos.z);

                    Vector3 sourceVector = new Vector3(source.x, source.y, source.z);
                    EditTerrain.SetBlock(source.x, source.y, source.z, hit, new BlockWater());

                    Chunk chunk = hit.collider.GetComponent<Chunk>();

                    StartCoroutine(chunk.world.SimulateWater(source, sourceVector, 4));
                }
            }
        }

    }    
}