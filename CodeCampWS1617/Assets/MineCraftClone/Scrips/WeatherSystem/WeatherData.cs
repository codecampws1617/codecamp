﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public struct WeatherData { 

    public string name;
    public float frequency;
    public ParticleSystem particleSystem;


};
