﻿using UnityEngine;
using System.Collections;

public class BlockCactus : Block {


    //override TexturePosition
    public override Tile TexturePosition(Direction direction)
    {
        Tile tile = new Tile();

        switch (direction)
        {
            case Direction.up:
                tile.x = 5;
                tile.y = 11;
                return tile;
            case Direction.down:
                tile.x = 5;
                tile.y = 11;
                return tile;
        }

        tile.x = 6;
        tile.y = 11;

        return tile;
    }

}
