﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class BlockAir : Block{
    
    #region Constructors
    public BlockAir()
        : base()
    {
        waterResistant = false;
    }
    #endregion Constructors

    //Needs to be override
    public override MeshData Blockdata
        (Chunk chunk, int x, int y, int z, MeshData meshData)
    {
        return meshData;
    }

    //Return "false" because air is not solid
    public override bool IsSolid(Block.Direction direction)
    {
        return false;
    }
}