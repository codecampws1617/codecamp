﻿using UnityEngine;
using System.Collections;
using System;
using Random = UnityEngine.Random;

[Serializable]
public class BlockGrass : Block
{
    public override Tile TexturePosition(Direction direction)
    {
        Tile tile = new Tile();

        switch (direction)
        {
            case Direction.up:
                tile.x = 0;
                tile.y = 15;

                return tile;
            case Direction.down:
                tile.x = 2;
                tile.y = 15;
                return tile;
        }

        tile.x = 3;
        tile.y = 15;

        return tile;
    }
}