﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class BlockWood : Block
{

    //override the texture position
    public override Tile TexturePosition(Direction direction)
    {
        Tile tile = new Tile();

        switch (direction)
        {
            case Direction.up:
                tile.x = 5;
                tile.y = 14;
                return tile;
            case Direction.down:
                tile.x = 5;
                tile.y = 14;
                return tile;
        }

        tile.x = 4;
        tile.y = 14;

        return tile;
    }

    //override FaceData functions to change the Block width to 0.2
    #region FaceData funcitons
    protected override MeshData FaceDataUp
        (Chunk chunk, int x, int y, int z, MeshData meshData)
    {
        meshData.AddVertex(new Vector3(x - 0.5f, y - 0.2f, z - 0.2f));
        meshData.AddVertex(new Vector3(x - 0.2f, y - 0.2f, z - 0.2f));
        meshData.AddVertex(new Vector3(x - 0.2f, y - 0.2f, z - 0.5f));
        meshData.AddVertex(new Vector3(x - 0.5f, y - 0.2f, z - 0.5f));

        meshData.AddQuadTriangles();
        meshData.uv.AddRange(FaceUVs(Direction.up));
        return meshData;
    }

    protected override MeshData FaceDataDown
        (Chunk chunk, int x, int y, int z, MeshData meshData)
    {
        meshData.AddVertex(new Vector3(x - 0.5f, y - 0.5f, z - 0.5f));
        meshData.AddVertex(new Vector3(x - 0.2f, y - 0.5f, z - 0.5f));
        meshData.AddVertex(new Vector3(x - 0.2f, y - 0.5f, z - 0.2f));
        meshData.AddVertex(new Vector3(x - 0.5f, y - 0.5f, z - 0.2f));

        meshData.AddQuadTriangles();
        meshData.uv.AddRange(FaceUVs(Direction.down));
        return meshData;
    }

    protected override MeshData FaceDataNorth
        (Chunk chunk, int x, int y, int z, MeshData meshData)
    {
        meshData.AddVertex(new Vector3(x - 0.2f, y - 0.5f, z - 0.2f));
        meshData.AddVertex(new Vector3(x - 0.2f, y + 0.5f, z - 0.2f));
        meshData.AddVertex(new Vector3(x - 0.5f, y + 0.5f, z - 0.2f));
        meshData.AddVertex(new Vector3(x - 0.5f, y - 0.5f, z - 0.2f));

        meshData.AddQuadTriangles();
        meshData.uv.AddRange(FaceUVs(Direction.north));
        return meshData;
    }

    protected override MeshData FaceDataEast
        (Chunk chunk, int x, int y, int z, MeshData meshData)
    {
        meshData.AddVertex(new Vector3(x - 0.2f, y - 0.5f, z - 0.5f));
        meshData.AddVertex(new Vector3(x - 0.2f, y + 0.5f, z - 0.5f));
        meshData.AddVertex(new Vector3(x - 0.2f, y + 0.5f, z - 0.2f));
        meshData.AddVertex(new Vector3(x - 0.2f, y - 0.5f, z - 0.2f));

        meshData.AddQuadTriangles();
        meshData.uv.AddRange(FaceUVs(Direction.east));
        return meshData;
    }

    protected override MeshData FaceDataSouth
        (Chunk chunk, int x, int y, int z, MeshData meshData)
    {
        meshData.AddVertex(new Vector3(x - 0.5f, y - 0.5f, z - 0.5f));
        meshData.AddVertex(new Vector3(x - 0.5f, y + 0.5f, z - 0.5f));
        meshData.AddVertex(new Vector3(x - 0.2f, y + 0.5f, z - 0.5f));
        meshData.AddVertex(new Vector3(x - 0.2f, y - 0.5f, z - 0.5f));

        meshData.AddQuadTriangles();
        meshData.uv.AddRange(FaceUVs(Direction.south));
        return meshData;
    }

    protected override MeshData FaceDataWest
        (Chunk chunk, int x, int y, int z, MeshData meshData)
    {
        meshData.AddVertex(new Vector3(x - 0.5f, y - 0.5f, z - 0.2f));
        meshData.AddVertex(new Vector3(x - 0.5f, y + 0.5f, z - 0.2f));
        meshData.AddVertex(new Vector3(x - 0.5f, y + 0.5f, z - 0.5f));
        meshData.AddVertex(new Vector3(x - 0.5f, y - 0.5f, z - 0.5f));

        meshData.AddQuadTriangles();
        meshData.uv.AddRange(FaceUVs(Direction.west));
        return meshData;
    }
    #endregion //FaceData functions
}