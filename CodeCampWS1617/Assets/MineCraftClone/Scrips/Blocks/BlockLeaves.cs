﻿using UnityEngine;
using System.Collections;
using System;
using Random = UnityEngine.Random;

[Serializable]
public class BlockLeaves : Block
{
    public override Tile TexturePosition(Direction direction)
    {
        Tile tile = new Tile();

        tile.x = 4;
        tile.y = 7;

        return tile;
    }

    public override bool IsSolid(Direction direction)
    {
        return false;
    }
}