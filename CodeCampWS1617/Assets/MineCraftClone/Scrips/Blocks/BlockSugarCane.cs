﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class BlockSugarCane : BlockIceFlower
{

    //overrides Texture Position
    public override Tile TexturePosition(Direction direction)
    {
        Tile tile = new Tile();

        switch (direction)
        {
            case Direction.up:
                tile.x = 6;
                tile.y = 12;
                return tile;
            case Direction.down:
                tile.x = 6;
                tile.y = 12;
                return tile;
        }

        tile.x = 9;
        tile.y = 11;

        return tile;
    }
}


