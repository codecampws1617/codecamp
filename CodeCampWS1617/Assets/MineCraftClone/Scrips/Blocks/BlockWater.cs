﻿using System.Collections;
using System;
using UnityEngine;

[Serializable]
public class BlockWater : Block
{

    //overrides Blockdata prevent adding collision to the mesh
    #region Blockdata 
    public override MeshData Blockdata
     (Chunk chunk, int x, int y, int z, MeshData meshData)
    {

        meshData.useRenderDataForCol = false;

        if (!chunk.GetBlock(x, y + 1, z).IsSolid(Direction.down))
        {
            meshData = FaceDataUp(chunk, x, y, z, meshData);
        }

        if (!chunk.GetBlock(x, y - 1, z).IsSolid(Direction.up))
        {
            meshData = FaceDataDown(chunk, x, y, z, meshData);
        }

        if (!chunk.GetBlock(x, y, z + 1).IsSolid(Direction.south))
        {
            meshData = FaceDataNorth(chunk, x, y, z, meshData);
        }

        if (!chunk.GetBlock(x, y, z - 1).IsSolid(Direction.north))
        {
            meshData = FaceDataSouth(chunk, x, y, z, meshData);
        }

        if (!chunk.GetBlock(x + 1, y, z).IsSolid(Direction.west))
        {
            meshData = FaceDataEast(chunk, x, y, z, meshData);
        }

        if (!chunk.GetBlock(x - 1, y, z).IsSolid(Direction.east))
        {
            meshData = FaceDataWest(chunk, x, y, z, meshData);
        }

        return meshData;

    }
    #endregion //Blockdata

    //Return "false" because water is not solid
    public override bool IsSolid(Block.Direction direction)
    {
        return false;
    }

    //override the texture position
    public override Tile TexturePosition(Direction direction)
    {
        Tile tile = new Tile();

        tile.x = 15;
        tile.y = 3;

        return tile;
    }
}