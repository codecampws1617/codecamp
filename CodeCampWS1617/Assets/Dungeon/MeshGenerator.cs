﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;



/************************************************************************************************************************************************************
 * Die ganze Map besteht aus Kacheln die entweder an sind (1) und eine Wand bilden, oder ausgeschaltet sind (0) und einen Leerraum bilden.
 * Nun ist jede Kachel ein Quadrat mit 4 Eckpunkten und 4 mittleren Punkten zwischen den Kanten.
 * Jeder Eckpunkt kann geschaltet werden (Zustand 1 oder 0 annehmen). Damit repräsentieren sie einen vierstelligen Binärcode, der eine dezimale Zahl ergibt.
 * 1----o----2
 * |         |
 * o         o
 * |         |
 * 4----o----3
 * 0000 = 0. Alle Eckpunkte sind aus.
 * 1000 = 8. Punkt 1 ist an, die anderen sind aus.
 * 1100 = 12. Punkt 1 und 2 sind an. Alle anderen sind aus (3 und 4).
 * 1111 = 15. Alle Punkte sind an.
 * Es gibt 16 Kombinationen.
 * Zwischen den Punkten 1 2 3 und 4 gibt es Zwischenpunkte, die benutzt werden um Dreiecke zu zeichnen. 15 = ein Quadrat ausgezeichnet.
 * 1 2 3 und 4 heißen ControllNodes. Jeder ControlNode hat zwei Nodes (Center Node) als Nachbar. Die Mittelpunkte heißen Nodes.
 * *********************************************************************************************************************************************************/


public class MeshGenerator : MonoBehaviour
{

    public SquareGrid squareGrid;
    public MeshFilter walls;
    public MeshFilter ground;

    List<Vector3> vertices; //Liste für alle Eckpunkte
    List<int> triangles;    //Liste für Dreiecke (ControlNode mit seinen CenterNodes verbunden)

    // Dictionary um später herauszufinden zu welchen Dreiecken die Eckpunkte gehören
    Dictionary<int, List<Triangle>> triangleDictionary = new Dictionary<int, List<Triangle>>(); 
    List<List<int>> outlines = new List<List<int>>();
    HashSet<int> checkedVertices = new HashSet<int>();

    //MeshGrid anhand der Map Größe und Kachel Größe erstellen.
    public void GenerateMesh(int[,] map, float squareSize)
    {

        triangleDictionary.Clear();
        outlines.Clear();
        checkedVertices.Clear();

        squareGrid = new SquareGrid(map, squareSize);

        vertices = new List<Vector3>();
        triangles = new List<int>();

        for (int x = 0; x < squareGrid.squares.GetLength(0); x++)
        {
            for (int y = 0; y < squareGrid.squares.GetLength(1); y++)
            {
                TriangulateSquare(squareGrid.squares[x, y]);
            }
        }

        Mesh mesh = new Mesh();
        GetComponent<MeshFilter>().mesh = mesh;

        mesh.vertices = vertices.ToArray(); // von Liste zu Array konvertieren.
        mesh.triangles = triangles.ToArray();
        mesh.RecalculateNormals();

        float wallHeight = 15;

        CreateWallMesh(wallHeight);
        CreateGroundMesh(wallHeight);
    }

    //Wände ziehen (Y-Achse)
    void CreateWallMesh(float wallHeight)
    {

        CalculateMeshOutlines();

        List<Vector3> wallVertices = new List<Vector3>();
        List<int> wallTriangles = new List<int>();
        Mesh wallMesh = new Mesh();

        foreach (List<int> outline in outlines)
        {
            // Durch alle Eckpunkte der Außenlinie gehen...
            for (int i = 0; i < outline.Count - 1; i++)     // -1 um OutOfBounce-Error zu vermeiden 
            {
                int startIndex = wallVertices.Count;
                wallVertices.Add(vertices[outline[i]]); // links
                wallVertices.Add(vertices[outline[i + 1]]); // rechts
                wallVertices.Add(vertices[outline[i]] - Vector3.up * wallHeight); // unten links
                wallVertices.Add(vertices[outline[i + 1]] - Vector3.up * wallHeight); // unten rechts

                wallTriangles.Add(startIndex + 0);
                wallTriangles.Add(startIndex + 2);
                wallTriangles.Add(startIndex + 3);

                wallTriangles.Add(startIndex + 3);
                wallTriangles.Add(startIndex + 1);
                wallTriangles.Add(startIndex + 0);
            }
        }
        wallMesh.vertices = wallVertices.ToArray();
        wallMesh.triangles = wallTriangles.ToArray();
        walls.mesh = wallMesh;
    }

    void CreateGroundMesh(float wallHeight)
    {

        List<Vector3> groundVertices = new List<Vector3>();
        List<int> groundTriangles = new List<int>();
        List<Vector2> groundUVs = new List<Vector2>();
        Mesh groundMesh = new Mesh();

        //Boden von Eckpunkt zu Eckpunkt erstellen
        Vector3 P0 = new Vector3(-squareGrid.squares.GetLength(0) / 2 - 0.5f, -1f * wallHeight, -squareGrid.squares.GetLength(1) / 2 - 0.5f);
        Vector3 P1 = new Vector3(squareGrid.squares.GetLength(0) / 2 + 0.5f, -1 * wallHeight, -squareGrid.squares.GetLength(1) / 2 - 0.5f);
        Vector3 P2 = new Vector3(squareGrid.squares.GetLength(0) / 2 + 0.5f, -1 * wallHeight, squareGrid.squares.GetLength(1) / 2 + 0.5f);
        Vector3 P3 = new Vector3(-squareGrid.squares.GetLength(0) / 2 - 0.5f, -1 * wallHeight, squareGrid.squares.GetLength(1) / 2 + 0.5f);

        groundVertices.Add(P0);
        groundVertices.Add(P1);
        groundVertices.Add(P2);
        groundVertices.Add(P3);

        groundTriangles.Add(3);
        groundTriangles.Add(2);
        groundTriangles.Add(1);

        groundTriangles.Add(3);
        groundTriangles.Add(1);
        groundTriangles.Add(0);

        groundMesh.vertices = groundVertices.ToArray();
        groundMesh.triangles = groundTriangles.ToArray();
        groundMesh.uv = groundUVs.ToArray();
        ground.mesh = groundMesh;
    }

    void TriangulateSquare(Square square)
    {
        switch (square.configuration)
        {
            case 0: //Keine Punkte verbunden -> kein Mesh.
                break;

            // 1 Point Cases: Jeweils ein ControlNode aktiv:
            case 1: //ControlNode unten links mit seinen CenterNodes verbunden und bilden ein Dreieck.
                MeshFromPoints(square.centreLeft, square.centreBottom, square.bottomLeft);
                break;
            case 2:
                MeshFromPoints(square.bottomRight, square.centreBottom, square.centreRight);
                break;
            case 4:
                MeshFromPoints(square.topRight, square.centreRight, square.centreTop);
                break;
            case 8:
                MeshFromPoints(square.topLeft, square.centreTop, square.centreLeft);
                break;

            // 2 Point Cases: Zwei ControlNodes miteinander verbunden
            case 3: // Unten rechts mit unten links und deren Center Punkten
                MeshFromPoints(square.centreRight, square.bottomRight, square.bottomLeft, square.centreLeft);
                break;
            case 6:
                MeshFromPoints(square.centreTop, square.topRight, square.bottomRight, square.centreBottom);
                break;
            case 9:
                MeshFromPoints(square.topLeft, square.centreTop, square.centreBottom, square.bottomLeft);
                break;
            case 12:
                MeshFromPoints(square.topLeft, square.topRight, square.centreRight, square.centreLeft);
                break;
            case 5:
                MeshFromPoints(square.centreTop, square.topRight, square.centreRight, square.centreBottom, square.bottomLeft, square.centreLeft);
                break;
            case 10:
                MeshFromPoints(square.topLeft, square.centreTop, square.centreRight, square.bottomRight, square.centreBottom, square.centreLeft);
                break;

            // 3 Point-Cases: Drei ControlNodes miteinander verbunden.
            case 7:
                MeshFromPoints(square.centreTop, square.topRight, square.bottomRight, square.bottomLeft, square.centreLeft);
                break;
            case 11:
                MeshFromPoints(square.topLeft, square.centreTop, square.centreRight, square.bottomRight, square.bottomLeft);
                break;
            case 13:
                MeshFromPoints(square.topLeft, square.topRight, square.centreRight, square.centreBottom, square.bottomLeft);
                break;
            case 14:
                MeshFromPoints(square.topLeft, square.topRight, square.bottomRight, square.centreBottom, square.centreLeft);
                break;

            // 4 Point-Case: Alle ControlNodes verbunden. Ganzes Quadrat.
            case 15:
                MeshFromPoints(square.topLeft, square.topRight, square.bottomRight, square.bottomLeft);
                checkedVertices.Add(square.topLeft.vertexIndex);
                checkedVertices.Add(square.topRight.vertexIndex);
                checkedVertices.Add(square.bottomRight.vertexIndex);
                checkedVertices.Add(square.bottomLeft.vertexIndex);
                break;
        }
    }

    //Meshs anhand der Punkte eines Dreiecks erstellen. | Beliebig viele Punkte übergeben können (params)
    void MeshFromPoints(params Node[] points)   
    {
        AssignVertices(points);

        if (points.Length >= 3) // ein Dreieck
            CreateTriangle(points[0], points[1], points[2]);
        if (points.Length >= 4) // zwei Dreiecke
            CreateTriangle(points[0], points[2], points[3]);
        if (points.Length >= 5) // drittes Dreieck
            CreateTriangle(points[0], points[3], points[4]);
        if (points.Length >= 6) // viertes Dreieck für die diagonalen Verbindungen zwischen Punkten.
            CreateTriangle(points[0], points[4], points[5]);

    }

    void AssignVertices(Node[] points)
    {
        for (int i = 0; i < points.Length; i++)
        {
            if (points[i].vertexIndex == -1)    //Default ist -1
            {
                points[i].vertexIndex = vertices.Count;
                vertices.Add(points[i].position);   // Position des Punktes durchgeben
            }
        }
    }

    //Dreieck aus den Indizes der Eckpunktliste erstellen
    void CreateTriangle(Node a, Node b, Node c) 
    {
        triangles.Add(a.vertexIndex);
        triangles.Add(b.vertexIndex);
        triangles.Add(c.vertexIndex);

        Triangle triangle = new Triangle(a.vertexIndex, b.vertexIndex, c.vertexIndex);
        AddTriangleToDictionary(triangle.vertexIndexA, triangle);
        AddTriangleToDictionary(triangle.vertexIndexB, triangle);
        AddTriangleToDictionary(triangle.vertexIndexC, triangle);
    }

    void AddTriangleToDictionary(int vertexIndexKey, Triangle triangle)
    {
        if (triangleDictionary.ContainsKey(vertexIndexKey))
        {
            triangleDictionary[vertexIndexKey].Add(triangle);
        }
        else
        {
            List<Triangle> triangleList = new List<Triangle>();
            triangleList.Add(triangle);
            triangleDictionary.Add(vertexIndexKey, triangleList);
        }
    }

    /* Durch alle Eckpunkte in der Map gehen und prüfen ob es sich beim aktuellen um ein Outline-Eckpunkt handelt.
    ** Wenn ja: Dieser Außenlinie folgen, bis man bei dem gleichen Eckpunkt wieder ankommt.
    ** Danach: diesen in die Outline-List hinzufügen.*/
    void CalculateMeshOutlines()
    {

        for (int vertexIndex = 0; vertexIndex < vertices.Count; vertexIndex++)
        {
            if (!checkedVertices.Contains(vertexIndex))
            {
                int newOutlineVertex = GetConnectedOutlineVertex(vertexIndex);
                if (newOutlineVertex != -1)
                {
                    checkedVertices.Add(vertexIndex);

                    List<int> newOutline = new List<int>();
                    newOutline.Add(vertexIndex);
                    outlines.Add(newOutline);
                    FollowOutline(newOutlineVertex, outlines.Count - 1);
                    outlines[outlines.Count - 1].Add(vertexIndex);
                }
            }
        }
    }

    //Außenkanten der Kacheln folgen
    void FollowOutline(int vertexIndex, int outlineIndex)
    {
        outlines[outlineIndex].Add(vertexIndex);
        checkedVertices.Add(vertexIndex);
        int nextVertexIndex = GetConnectedOutlineVertex(vertexIndex);

        if (nextVertexIndex != -1)  // Weiter der Outline folgen.
        {
            FollowOutline(nextVertexIndex, outlineIndex);
        }
    }

    // Mit einem gegebenen Outline-Eckpunkt einen verbunden Eckpunkt finden, der die Außenlinie formt. | Gibt int als Index des Eckpunktes
    int GetConnectedOutlineVertex(int vertexIndex)
    {
        List<Triangle> trianglesContainingVertex = triangleDictionary[vertexIndex];

        for (int i = 0; i < trianglesContainingVertex.Count; i++)
        {
            Triangle triangle = trianglesContainingVertex[i];

            for (int j = 0; j < 3; j++)
            {
                int vertexB = triangle[j];
                if (vertexB != vertexIndex && !checkedVertices.Contains(vertexB))
                {
                    if (IsOutlineEdge(vertexIndex, vertexB))
                    {
                        return vertexB;
                    }
                }
            }
        }

        return -1;
    }

    /* Prüfen ob die Linie zwischen zwei Eckpunkten eine Outline ist.
    ** Verfahren: Liste aller Dreiecke zu denen Eckpunkt A gehört durchgehen und prüfen wie viele dieser Dreiecke auch Eckpunkt B besitzen.
    ** Wenn Eckpunkt A und B nur ein Dreieck teilen, ist die Linie zwischen A und B eine Außenlinie.*/
    bool IsOutlineEdge(int vertexA, int vertexB)
    {
        List<Triangle> trianglesContainingVertexA = triangleDictionary[vertexA];
        int sharedTriangleCount = 0;

        for (int i = 0; i < trianglesContainingVertexA.Count; i++)
        {
            if (trianglesContainingVertexA[i].Contains(vertexB))
            {
                sharedTriangleCount++;
                if (sharedTriangleCount > 1)
                {
                    break;
                }
            }
        }
        return sharedTriangleCount == 1;
    }

    // Standardaufbau der Dreiecke
    struct Triangle
    {
        public int vertexIndexA;
        public int vertexIndexB;
        public int vertexIndexC;
        int[] vertices;

        public Triangle(int a, int b, int c)
        {
            vertexIndexA = a;
            vertexIndexB = b;
            vertexIndexC = c;

            vertices = new int[3];
            vertices[0] = a;
            vertices[1] = b;
            vertices[2] = c;
        }

        public int this[int i]
        {
            get
            {
                return vertices[i];
            }
        }


        public bool Contains(int vertexIndex)
        {
            return vertexIndex == vertexIndexA || vertexIndex == vertexIndexB || vertexIndex == vertexIndexC;
        }
    }

    public class SquareGrid
    {
        public Square[,] squares;

        public SquareGrid(int[,] map, float squareSize)
        {
            int nodeCountX = map.GetLength(0);
            int nodeCountY = map.GetLength(1);

            //Anzahl der Nodes auf der X-Achse multipliziert mit der Square-Größe - Bildet Map-Breite
            float mapWidth = nodeCountX * squareSize;

            //Anzahl der Nodes auf der Y-Achse multipliziert mit der Square-Größe - Bildet Map-Höhe
            float mapHeight = nodeCountY * squareSize;

            ControlNode[,] controlNodes = new ControlNode[nodeCountX, nodeCountY];

            for (int x = 0; x < nodeCountX; x++)
            {
                for (int y = 0; y < nodeCountY; y++)
                {
                    // Position der aktuellen ControlNode bestimmen
                    Vector3 pos = new Vector3(-mapWidth / 2 + x * squareSize + squareSize / 2, 0, -mapHeight / 2 + y * squareSize + squareSize / 2);

                    // ControlNode Position übergeben
                    controlNodes[x, y] = new ControlNode(pos, map[x, y] == 1, squareSize);
                }
            }

            //Wichtig: Es gibt immer einen Node weniger, als es einen Node gibt.
            squares = new Square[nodeCountX - 1, nodeCountY - 1];
            for (int x = 0; x < nodeCountX - 1; x++)
            {
                for (int y = 0; y < nodeCountY - 1; y++)
                {
                    //Durch jeden Square in dem Array gehen.
                    //Reihenfolge: oben links, oben rechts, unten rechts, unten links
                    squares[x, y] = new Square(controlNodes[x, y + 1], controlNodes[x + 1, y + 1], controlNodes[x + 1, y], controlNodes[x, y]);
                }
            }
        }
    }

    public class Square
    {
        //Alle Nodes an den Rändern einer Kachel
        public ControlNode topLeft, topRight, bottomRight, bottomLeft;

        //Center-Nodes zwischen den Eckpunkten
        public Node centreTop, centreRight, centreBottom, centreLeft;
        public int configuration;

        //Ein Square bildet sich aus allen Eckpunkten
        public Square(ControlNode _topLeft, ControlNode _topRight, ControlNode _bottomRight, ControlNode _bottomLeft)
        {
            topLeft = _topLeft;
            topRight = _topRight;
            bottomRight = _bottomRight;
            bottomLeft = _bottomLeft;

            centreTop = topLeft.right;
            centreRight = bottomRight.above;
            centreBottom = bottomLeft.right;
            centreLeft = bottomLeft.above;

            /*Kontrollieren welche ControllNodes aktiviert sind.
             * 1----o----2
             * |         |
             * o         o
             * |         |
             * 4----o----3
             * ControlNodes als vierstelliegen Binärcode (1111) 1234, ergeben einen dezimalen Wert:
             * Beispiel: Nur ControlNode 1 ist aktiviert: 1000 = 8
             * ControlNode 2 aktiv = 4
             * ControlNode 3 aktiv = 2
             * ControlNode 4 aktiv = 1
             * */
            if (topLeft.active)
                configuration += 8;
            if (topRight.active)
                configuration += 4;
            if (bottomRight.active)
                configuration += 2;
            if (bottomLeft.active)
                configuration += 1;
        }
    }

    //Basis Klasse aller Nodes
    public class Node
    {
        public Vector3 position;
        public int vertexIndex = -1;

        public Node(Vector3 _pos)
        {
            position = _pos;
        }
    }

    //Klasse der Eckpunkte
    public class ControlNode : Node
    {

        public bool active;
        public Node above, right;

        public ControlNode(Vector3 _pos, bool _active, float squareSize) : base(_pos)
        {
            active = _active;

            //Position der aktiven Control-Nodes
            above = new Node(position + Vector3.forward * squareSize / 2f);
            right = new Node(position + Vector3.right * squareSize / 2f);
        }
    }
}
 